Coq-au-Vim
==========

Coq-au-Vim is a Vim plugin for working with the [Coq](https://coq.inria.fr/)
proof assistant. It provides motions, folding and communication with Coq for
interactive proof development.


Requirements
------------

Coq-au-Vim requires

- [Vim](https://www.vim.org/) version 8 or above, or [Neovim](https://neovim.io/)
- Coq version 8.6 or above

Prior versions of Vim will not be supported because version 8 is the one that
introduces asynchronous communication with external tools. Prior versions of
Coq will not be supported either because the interaction protocol has changed
completely with version 8.6. For older versions of Vim and Coq, use
[Coquille](https://github.com/the-lambda-church/coquille), which works well
with Coq up to version 8.5.


Installation
------------

The repository at <https://framagit.org/manu/coq-au-vim.git> is directly
usable as a plugin. Use the package manager of your choice to install it.

Syntax highlighting and indent scripts are not included, I recommend using
those by Vincent Aravantinos and packaged by Jeremy Voorhis at
<https://github.com/jvoorhis/coq.vim>.


Usage
-----

Full documentation is included with the plugin, say `:help coq.txt` from
within Vim to display the help file. For interaction with Coq, the following
commands are provided:

- `:CoqStart` starts an interactive session and defines the other commands,
- `:CoqQuit` ends the current interactive session,
- `:CoqNext` sends the next sentence to Coq,
- `:CoqRewind` rewinds by one? sentence,
- `:CoqToCursor` sends or rewinds up to the current cursor position,
- `:CoqQuery {text}` sends a query in the current state,
- `:CoqSet {options}` sets interpreter options.

Key mappings compatible with CoqIDE are installed by the `:CoqStart`
command.
